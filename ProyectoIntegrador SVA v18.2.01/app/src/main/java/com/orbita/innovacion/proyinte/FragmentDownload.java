package com.orbita.innovacion.proyinte;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentDownload extends Fragment {

    CircleImageView img1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment_download, container, false);
        img1 = (CircleImageView) v.findViewById(R.id.photo1);

        Glide.with(this)
                .load("https://drive.google.com/uc?export=download&id=1yu690tIQ-J-nl8TW6AMwzTA80O-3-qyc")
                .into(img1);

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://drive.google.com/uc?export=download&id=1yu690tIQ-J-nl8TW6AMwzTA80O-3-qyc");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        return v;
    }

}
