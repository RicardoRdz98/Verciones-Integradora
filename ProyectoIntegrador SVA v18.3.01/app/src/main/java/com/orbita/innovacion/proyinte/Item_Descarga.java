package com.orbita.innovacion.proyinte;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

/**
 * Clase de inicilizacion y obtencion de datos (POJO) para el manejo de los datos de los horarios
 * traidos desde la base de datos.
 *
 * Fecha de creación: 14/02/2018.
 * Versión: 18.2.15
 * Modificaciones:
 * Integracion de decodificacion de Base64 a Bitmap 14/02/2018.
 */

public class Item_Descarga {

    private String img;
    private String url_img;
    private String titulo;
    private String contenido;

    public Item_Descarga(String titulo, String contenido, String img, String url_img) {
        this.img = img;
        this.url_img = url_img;
        this.titulo = titulo;
        this.contenido = contenido;
    }

    public Bitmap getImg() {
        return base64ToBitmap(img);
    }

    public String getTitulo() {
        return titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public String getUrl_img() {
        return url_img;
    }

    private Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }
    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

}
