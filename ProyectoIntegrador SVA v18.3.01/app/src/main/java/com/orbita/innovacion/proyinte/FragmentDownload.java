package com.orbita.innovacion.proyinte;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase de Fragmento encargada de poder realizar descargas de cada uno de los horarios según el aula
 * que le haya tocado al alumno.
 *
 * Fecha de creación: 15/01/2018.
 * Versión: 18.2.15
 * Modificaciones:
 * Adaptacion de CardView y RecyclerView para la visualizacion y descarga de horarios de clase 14/02/2018
 * Integracion de funcionavilidad a boton de descarga de la CardView 15/02/2018
 */

public class FragmentDownload extends Fragment {

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    private RecyclerView.Adapter adapter;
    private RecyclerView recycler;
    private RecyclerView.LayoutManager lManager;

    private ArrayList<Item_Descarga> items = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /*Método que realizará las descargas de los horarios de los alumnos
    * registrados en una aula de clases*/
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment_download, container, false);

        items = new ArrayList<Item_Descarga>();

        // Obtener el RecyclerView
        recycler = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        recycler.setHasFixedSize(true);

        // Llamada a la coleccion de Horarios para traer todos los datos
        db.collection("Horarios").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (DocumentSnapshot document : task.getResult()) {
                                items.add(new Item_Descarga(document.getString("grupo"),
                                        document.getString("contenido"),
                                        document.getString("img"),
                                        document.getString("url_img")));
                            }

                            // Usar un administrador para LinearLayout
                            lManager = new LinearLayoutManager(getContext());
                            recycler.setLayoutManager(lManager);

                            // Crear un nuevo adaptador
                            adapter = new AdapterListItem(items);
                            recycler.setAdapter(adapter);
                        } else {

                        }
                    }
                });

        return v;
    }

    //Clase anidada para adaptar datos al CardView y RecyclerView
    public class AdapterListItem extends RecyclerView.Adapter<AdapterListItem.HorarioViewHolder> {
        //Creacion de lista de items
        private List<Item_Descarga> items;

        public class HorarioViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            // Campos respectivos de un item
            private ImageView imagen;
            private TextView titulo;
            private TextView contenido;
            private Button descargar;
            private TextView url_img;
            private Context context;

            public HorarioViewHolder(View v) {
                //Inicilizando WidGets del CardView (item_descargas)
                super(v);
                context = v.getContext();
                url_img = (TextView) v.findViewById(R.id.txt_url_img);
                imagen = (ImageView) v.findViewById(R.id.imgCard);
                titulo = (TextView) v.findViewById(R.id.txtTituloCard);
                contenido = (TextView) v.findViewById(R.id.txtConteCard);
                descargar = (Button) v.findViewById(R.id.btmDescargar);
            }
            public void accesoAbotones(){
                descargar.setOnClickListener(this);
            }

            // Boton de descarga de imagen de horario para su mejor acceso
            @Override
            public void onClick(View v) {
                if(v.getId() == R.id.btmDescargar){
                    // Aqui se obendra el link de descarga de la imagen y lanzara aplicacion
                    // o pagina que mejor le convenga a la escuela
                    Uri uri = Uri.parse(url_img.getText().toString());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
            }
        }

        public AdapterListItem(List<Item_Descarga> items) {
            //Igualando datos de la lista
            this.items = items;
        }

        @Override
        public int getItemCount() {
            //Sacando tamaño de la lista
            return items.size();
        }

        @Override
        public HorarioViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            //Inflando Layout de CardView (item_descargas)
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_descargas, viewGroup, false);
            return new HorarioViewHolder(v);
        }

        @Override
        public void onBindViewHolder(HorarioViewHolder viewHolder, int i) {
            //Agregando los datos al CardView (item_descargas)
            //y dando paso a los eventos onClickListener del Boton descargar
            viewHolder.imagen.setImageBitmap(items.get(i).getImg());
            viewHolder.url_img.setText(items.get(i).getUrl_img());
            viewHolder.titulo.setText(items.get(i).getTitulo());
            viewHolder.contenido.setText(items.get(i).getContenido());
            viewHolder.accesoAbotones();
        }
    }

}
