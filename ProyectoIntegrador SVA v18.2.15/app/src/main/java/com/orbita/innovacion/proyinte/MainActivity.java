package com.orbita.innovacion.proyinte;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Clase encargada del total manejo de la aplicación, encargada de desplegar cada uno de los fragmentos
 * aquí implementados para las demas acciones.
 *
 * Created by Ricardo Rdz, Yarely Gmz on 17/01/2018.
 * Versión: 18.2.08
 * Advertencias: ¡Clase en constante cambio!
 * Modificaciones: Integración de métodos para contactar cn los programadores on 02/02/2018
 * Integración de diseño para poder ingresar Pin de seguridad en la opción Pagos on 10/02/2018
 */

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener {

    private String correo = "yagari2017@gmail.com", contrasena = "YAGARIINC2017";
    private String a = "null", b = "null";
    private TextView mensaje, address, nombre;
    private Button enviar;
    private Session session;
    private LottieAnimationView Check;
    private TextInputLayout Lmensaje, Laddress, Lnombre;

    private CoordinatorLayout coordinatorLayout;

    private CircleImageView photo;
    private TextView name, email;
    private GoogleApiClient googleApiClient;

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    private int snackBar = 0;
    private String PINPassword = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= 21){
            getWindow().setNavigationBarColor(getResources().getColor(R.color.black));
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

//Inicio: by milton
        View headerView = navigationView.getHeaderView(0);
        photo = (CircleImageView) headerView.findViewById(R.id.photo);
        name  = (TextView) headerView.findViewById(R.id.nametextview);
        email = (TextView) headerView.findViewById(R.id.emailtextview);
//Fin
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.fram);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user != null){
                    setUserData(user);
                }else{
                    goLogInScreen();
                }
            }
        };

        /*FRAGMENTO HOME AL INICIAR LA ACTIVITY*/
        FragmentHome fragment1 = new FragmentHome();
        FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
        fragmentTransaction1.replace(R.id.fram, fragment1, "Fragment Home");  //create first framelayout with id fram in the activity where fragments will be displayed
        fragmentTransaction1.commit();
    }

    @Override
    public void onBackPressed() {
        ventanaDialogo("Salir de la aplicación", "¿Seguro/a que desea salir?", "BackPressed");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.comentarios:
                display();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_eventos) {

        } else if (id == R.id.nav_home) {
            FragmentHome fragment1 = new FragmentHome();
            FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
            fragmentTransaction1.replace(R.id.fram, fragment1, "Fragment Home");
            fragmentTransaction1.commit();
        } else if (id == R.id.nav_mapas) {
            MapsActivity fragment2 = new MapsActivity();
            FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
            fragmentTransaction2.replace(R.id.fram, fragment2, "Fragment Map");
            fragmentTransaction2.commit();
        } else if (id == R.id.nav_comedor) {

        } else if (id == R.id.nav_pagos) {
            IncertarPin();
        } else if(id == R.id.nav_descarga){
            FragmentDownload fragment4 = new FragmentDownload();
            FragmentTransaction fragmentTransaction4 = getSupportFragmentManager().beginTransaction();
            fragmentTransaction4.replace(R.id.fram, fragment4, "Fragment Download");
            fragmentTransaction4.commit();
        } else if (id == R.id.nav_ajustes) {

        } else if (id == R.id.nav_logout) {
            ventanaDialogo("Cerrar Sesión", "¿Seguro/a que desea cerrar sesión?", "LogOut");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void ventanaDialogo(String titulo, String mensaje, final String intencion)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setTitle(titulo);
        builder.setMessage(mensaje);

        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Snackbar.make(coordinatorLayout, "Accion Desechada",
                                Snackbar.LENGTH_LONG).setAction("", null).show();

                    }
                });

        builder.setPositiveButton("Si",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if(intencion.toString().equals("LogOut")){

                            LogOut();

                        }else if(intencion.toString().equals("BackPressed")){

                            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                            if (drawer.isDrawerOpen(GravityCompat.START)) {
                                drawer.closeDrawer(GravityCompat.START);
                            }
                            finish();
                        }

                    }
                });
        builder.show();

    }

    /*EMPIESA CODIGO DE FIREBASE Y CUENTA DE GOOGLE*/

    private void setUserData(FirebaseUser user) {
        name.setText(user.getDisplayName());
        email.setText(user.getEmail());

        Glide.with(this)
                .load(user.getPhotoUrl())
                .into(photo);

        if(snackBar == 0){
            Snackbar.make(coordinatorLayout, "Bienvenido " + user.getDisplayName(),
                    Snackbar.LENGTH_LONG).setAction("", null).show();
            snackBar++;
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.putString("IDGoogle", user.getUid());
        editor.putString("Nombre", user.getDisplayName());
        editor.putString("Email", user.getEmail());
        editor.apply();

    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    private void goLogInScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void LogOut(){
        firebaseAuth.signOut();
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if(status.isSuccess()){
                    goLogInScreen();
                }else{
                    Snackbar.make(coordinatorLayout, "No se pudo cerrar sesión",
                            Snackbar.LENGTH_LONG).setAction("", null).show();
                }
            }
        });
    }


    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Snackbar.make(coordinatorLayout, "Conexión Fallida",
                Snackbar.LENGTH_LONG).setAction("", null).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(firebaseAuthListener != null){
            firebaseAuth.removeAuthStateListener(firebaseAuthListener);
        }
    }

    /*EMPIESA CODIGO DE CONTACTAR*/

    private void display() {
        Dialog d = new Dialog(this);
        d.setContentView(R.layout.comentarios);
        final LinearLayout contacto;

        contacto = (LinearLayout) d.findViewById(R.id.Contacto);
        Check = (LottieAnimationView) d.findViewById(R.id.check);
        nombre = (EditText) d.findViewById(R.id.edtname);
        address = (EditText) d.findViewById(R.id.edtcorreo);
        mensaje = (EditText) d.findViewById(R.id.edtmensaje);
        enviar = (Button) d.findViewById(R.id.btmenviar);
        Lnombre = (TextInputLayout) d.findViewById(R.id.edtnameLayout);
        Laddress = (TextInputLayout) d.findViewById(R.id.edtcorreoLayout);
        Lmensaje = (TextInputLayout) d.findViewById(R.id.edtmensajeLayout);

        a = PreferenceManager.getDefaultSharedPreferences(this).getString("Nombre","");
        b = PreferenceManager.getDefaultSharedPreferences(this).getString("Email","");
        nombre.setText(a);
        address.setText(b);

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nombre.getText().toString().equals("")){
                    Snackbar.make(contacto, "Campo de Nombre vacío",
                            Snackbar.LENGTH_LONG).setAction("", null).show();
                    return;
                }else if(address.getText().toString().equals("")){
                    Snackbar.make(contacto, "Campo de Correo vacío",
                            Snackbar.LENGTH_LONG).setAction("", null).show();
                    return;
                }else if(mensaje.getText().toString().equals("")){
                    Snackbar.make(contacto, "Campo de Mensaje vacío",
                            Snackbar.LENGTH_LONG).setAction("", null).show();
                    return;
                }else {
                    mandaremail();
                }
            }
        });
        d.show();
    }

    private void mandaremail(){
        nombre.setVisibility(View.GONE);
        address.setVisibility(View.GONE);
        mensaje.setVisibility(View.GONE);
        enviar.setVisibility(View.GONE);
        Lnombre.setVisibility(View.GONE);
        Laddress.setVisibility(View.GONE);
        Lmensaje.setVisibility(View.GONE);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.googlemail.com");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", "465");

        try {
            session= Session.getDefaultInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(correo,contrasena);
                }
            });

            if(session != null){
                javax.mail.Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(correo));
                message.setSubject(a + " " + b);
                message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse("rodriguez231402@hotmail.com"));
                message.setContent(mensaje.getText().toString(), "text/html; charset=utf-8");
                Transport.send(message);

                Check.setVisibility(View.VISIBLE);
                Check.playAnimation();

            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /*EMPIEZA CODIGO DE PIN*/

    private void IncertarPin(){
        PINPassword = PreferenceManager.getDefaultSharedPreferences(this).getString("PINPassword","null");

        if(PINPassword.toString().equals("null")){
            Intent intent = new Intent(this, Pagos.class);
            startActivity(intent);
        }else{
            Dialog d = new Dialog(this);
            d.setContentView(R.layout.pin_layout);
            final LinearLayout pinLayout;
            pinLayout = (LinearLayout) d.findViewById(R.id.LayoutPin);

            final PinEntryEditText pinEntry = (PinEntryEditText) d.findViewById(R.id.txt_pin_entry);
            if (pinEntry != null) {
                pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                    @Override
                    public void onPinEntered(CharSequence str) {
                        if (str.toString().equals(PINPassword)) {
                            Intent intent = new Intent(MainActivity.this, Pagos.class);
                            startActivity(intent);
                        } else {
                            Snackbar.make(pinLayout, "Contraseña incorrecta",
                                    Snackbar.LENGTH_LONG).setAction("", null).show();
                            pinEntry.setText(null);
                        }
                    }
                });

                d.show();
            }
        }
    }

}
